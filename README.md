## NPAW
# Frontend

## Setup
```
git clone ..
cd npaw-frontend
npm install
```

## Run
```
npm start
```
Assumes `npaw-backend` is running on the same machine
import axios from 'axios'
import initialState from './state'

// GET
export const FETCH_PERMISSIONS_BEGIN = 'permissions/FETCH_PERMISSIONS_BEGIN'
export const FETCH_PERMISSIONS_SUCCESS = 'permissions/FETCH_PERMISSIONS_SUCCESS'
export const FETCH_PERMISSIONS_FAILURE = 'permissions/FETCH_PERMISSIONS_FAILURE'
// POST
export const POST_PERMISSIONS_BEGIN = 'permissions/POST_PERMISSIONS_BEGIN'
export const POST_PERMISSIONS_SUCCESS = 'permissions/POST_PERMISSIONS_SUCCESS'
export const POST_PERMISSIONS_FAILURE = 'permissions/POST_PERMISSIONS_FAILURE'
// DEL
export const DEL_PERMISSIONS_BEGIN = 'permissions/DEL_PERMISSIONS_BEGIN'
export const DEL_PERMISSIONS_SUCCESS = 'permissions/DEL_PERMISSIONS_SUCCESS'
export const DEL_PERMISSIONS_FAILURE = 'permissions/DEL_PERMISSIONS_FAILURE'
// OTHERS
export const CLEAR_PERMISSION_CREATED = 'permission/CLEAR_PERMISSION_CREATED'

// Reducers
export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PERMISSIONS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case FETCH_PERMISSIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        list: action.payload.permissions
      }
    case FETCH_PERMISSIONS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        list: []
      }
    case POST_PERMISSIONS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case POST_PERMISSIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        permissionCreated: action.payload.permission
      }
    case POST_PERMISSIONS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        list: []
      }
    case DEL_PERMISSIONS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case DEL_PERMISSIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        list: state.list.filter(item => item._id !== action.payload.id)
      }
    case DEL_PERMISSIONS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error
      }
    case CLEAR_PERMISSION_CREATED:
      return {
        ...state,
        loading: false,
        error: null,
        permissionCreated: undefined
      }
    default:
      return state
  }
}

// Action Creators
export const fetchPermissions = () => {
  return dispatch => {
    dispatch({
      type: FETCH_PERMISSIONS_BEGIN
    })

    return axios.get('http://localhost:8088/api/permissions').then(
      response => dispatch({ type: FETCH_PERMISSIONS_SUCCESS, payload: { permissions: response.data } }),
      error => dispatch({ type: FETCH_PERMISSIONS_FAILURE, payload: { error } })
    )
  }
}
export const postPermission = permission => {
  return dispatch => {
    dispatch({
      type: POST_PERMISSIONS_BEGIN
    })

    return axios.post('http://localhost:8088/api/permissions', {permission}, {headers: {'Content-Type': 'application/json'}}).then(
      response => dispatch({ type: POST_PERMISSIONS_SUCCESS, payload: { permission: response.data } }),
      error => dispatch({ type: POST_PERMISSIONS_FAILURE, payload: { error } })
    )
  }
}
export const delPermission = permission => {
  return dispatch => {
    dispatch({
      type: DEL_PERMISSIONS_BEGIN
    })

    return axios.delete(`http://localhost:8088/api/permissions/${permission._id}`).then(
      response => dispatch({ type: DEL_PERMISSIONS_SUCCESS, payload: { id: response.data } }),
      error => dispatch({ type: DEL_PERMISSIONS_FAILURE, payload: { error } })
    )
  }
}

export const clearPermissionCreated = () => {
  return dispatch => dispatch({type: CLEAR_PERMISSION_CREATED})
}

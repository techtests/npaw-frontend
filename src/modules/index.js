import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { reducer as formReducer } from 'redux-form'
import accounts from './accounts.module'
import permissions from './permissions.module'

export const initialState = {
  list: [],
  loading: false,
  success: false,
  error: null
}

export default combineReducers({
  accounts,
  permissions,
  form: formReducer,
  routing: routerReducer
})

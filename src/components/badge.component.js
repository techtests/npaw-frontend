import React from 'react'

export default props => {
  const badges = {
    Read: 'success',
    Write: 'warning',
    Execute: 'danger'
  }

  return (
    <span className={`badge badge-${badges[props.permission.name]} mr-1`}>{ props.permission.name }</span>
  )
}

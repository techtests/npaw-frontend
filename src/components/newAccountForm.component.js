import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'

class NewAccountForm extends Component {
  constructor (props) {
    super(props)
    this.values = {
      name: '',
      options: []
    }
  }

  componentWillReceiveProps (props) {
    if (props.permissions && props.permissions.length) {
      this.values.options = props.permissions.map(permission => <option key={permission._id} value={permission._id}>{permission.name}</option>)
    }
    if (props.account) {
      this.values.name = props.account.name
    }
  }

  render () {
    return (
      <form onSubmit={this.props.handleSubmit}>
        <div className='form-group'>
          <label htmlFor='name'>Account name</label>
          <div className='input-group mb-2'>
            <div className='input-group-prepend'>
              <div className='input-group-text'>{this.values.name}</div>
            </div>
            <Field component='input' name='name' type='text' required className='form-control' />
          </div>
          <label htmlFor='permissions' className='mt-3'>Permissions</label>
          <Field component='select' multiple className='form-control' name='permissions'>
            { this.values.options }
          </Field>
          <button className='btn btn-secondary mt-3' type='submit'>
            { this.props.onButton }
          </button>
        </div>
      </form>
    )
  }
}

export default reduxForm({
  form: 'newAccount'
})(NewAccountForm)

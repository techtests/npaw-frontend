import React from 'react'
import { NavLink } from 'react-router-dom'

export default () => {
  return (
    <nav className='navbar navbar-expand-lg navbar-dark bg-dark'>
      <a className='navbar-brand' href=''>NPAW</a>
      <ul className='navbar-nav mr-auto'>
        <li className='nav-item'>
          <NavLink className='nav-link' to='/accounts'>Accounts</NavLink>
        </li>
        <li className='nav-item'>
          <NavLink className='nav-link' to='/permissions'>Permissions</NavLink>
        </li>
      </ul>
    </nav>
  )
}

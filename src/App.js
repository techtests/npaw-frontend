import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { BrowserRouter as Router, Route } from 'react-router-dom'
// Components
import Header from './components/header.component'
// Containers
import Accounts from './containers/accounts.container'
import Account from './containers/account.container'
import Permissions from './containers/permissions.container'
import Permission from './containers/permission.container'

class App extends Component {
  render () {
    return (
      <Router>
        <div>
          <Header />
          <Route exact path='/accounts' component={Accounts} />
          <Route path='/accounts/:id' component={Account} />
          <Route exact path='/permissions' component={Permissions} />
          <Route path='/permissions/add' component={Permission} />
        </div>
      </Router>
    )
  }
}

export default App

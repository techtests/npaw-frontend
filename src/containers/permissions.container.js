import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchPermissions, delPermission } from '../modules/permissions.module'
import { Link } from 'react-router-dom'

import Permission from '../components/permission.component'

class Permissions extends Component {
  componentDidMount () {
    this.props.fetchPermissions()
    console.log('[Permissions] I did mount')
  }

  renderPermissions () {
    if (Array.isArray(this.props.permissions.list)) {
      return this.props.permissions.list.map(permission => <Permission key={permission._id} deleteClick={() => { this.props.delPermission(permission) }} permission={permission} />)
    }
  }

  render () {
    return (
      <div className='container'>
        <div className='row-justify-content-md-center mt-4'>
          <div className='col'>
            <Link className='btn btn-secondary mr-2' to='/permissions/add'>
              Add new permission
            </Link>
          </div>
        </div>
        <div className='row-justify-content-md-center mt-4'>
          <div className='col'>
            <table className='table'>
              <thead>
                <tr>
                  <th>Unique Identifier</th>
                  <th>Permission name</th>
                  <th>Description</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                { this.renderPermissions() }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
}

// Map redux state to props
function mapStateToProps (state) {
  return {
    permissions: state.permissions
  }
}
// Map actions to props
const mapDispatchToProps = dispatch => bindActionCreators({
  fetchPermissions,
  delPermission
}, dispatch)
// Promote Accounts from component to container
export default connect(mapStateToProps, mapDispatchToProps)(Permissions)

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchAccounts, delAccount } from '../modules/accounts.module'
import { Link } from 'react-router-dom'

import Account from '../components/account.component'

class Accounts extends Component {
  constructor (props) {
    super(props)
    this.selectedAccounts = []
  }
  componentDidMount () {
    this.props.fetchAccounts()
    console.log('[Accounts] I did mount')
  }

  renderAccounts () {
    if (Array.isArray(this.props.accounts.list)) {
      return this.props.accounts.list.map(account => <Account key={account._id} editClick={() => this.props.history.push(`/accounts/${account._id}`)} deleteClick={() => this.props.delAccount(account)} account={account} />)
    }
  }

  render () {
    return (
      <div className='container'>
        <div className='row-justify-content-md-center mt-4'>
          <div className='col'>
            <Link className='btn btn-secondary mr-2' to='/accounts/add'>
              Add new account
            </Link>
          </div>
        </div>
        <div className='row-justify-content-md-center mt-4'>
          <div className='col'>
            <table className='table'>
              <thead>
                <tr>
                  <th>Unique Identifier</th>
                  <th>Account name</th>
                  <th>Permissions</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                { this.renderAccounts() }
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
}

// Map redux state to props
function mapStateToProps (state) {
  return {
    accounts: state.accounts
  }
}
// Map actions to props
const mapDispatchToProps = dispatch => bindActionCreators({
  fetchAccounts,
  delAccount
}, dispatch)
// Promote Accounts from component to container
export default connect(mapStateToProps, mapDispatchToProps)(Accounts)
